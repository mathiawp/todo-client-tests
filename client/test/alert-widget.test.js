// @flow

import * as React from 'react';
import { Component } from 'react-simplified';
import { Alert } from '../src/widgets.js';
import { shallow } from 'enzyme';

class ThreeAlerts extends Component {
  render() {
    return (
      <div>
        <Alert.danger text="Error" />
        <Alert.success text="Success" />
        <Alert.info text="Info" />
      </div>
    );
  }
}

describe('Alert tests', () => {
  test('No alerts initially', () => {
    const wrapper = shallow(<Alert />);

    expect(wrapper.matchesElement(<></>)).toEqual(true);
  });

  test('Show alert message', (done) => {
    const wrapper = shallow(<Alert />);

    Alert.danger('test');

    // Wait for events to complete
    setTimeout(() => {
      expect(
        wrapper.matchesElement(
          <>
            <div>
              test<button>&times;</button>
            </div>
          </>
        )
      ).toEqual(true);

      done();
    });
  });

  test('Close alert message', (done) => {
    const wrapper = shallow(<Alert />);

    Alert.danger('test');

    // Wait for events to complete
    setTimeout(() => {
      expect(
        wrapper.matchesElement(
          <>
            <div>
              test<button>&times;</button>
            </div>
          </>
        )
      ).toEqual(true);

      wrapper.find('button.close').simulate('click');

      expect(wrapper.matchesElement(<></>)).toEqual(true);

      done();
    });
  });

  test('Open 3 alerts and close the second', (done) => {
    const wrapper = shallow(<Alert />);

    Alert.danger('error-test');
    Alert.success('success-test');
    Alert.info('info-test');

    // Wait for events to complete
    setTimeout(() => {
      expect(
        wrapper.containsMatchingElement(
          <>
            <div>
              error-test<button>&times;</button>
            </div>
            <div>
              success-test<button>&times;</button>
            </div>
            <div>
              info-test<button>&times;</button>
            </div>
          </>
        )
      ).toEqual(true);

      wrapper.find('.alert-success').find('button.close').simulate('click');

      expect(
        wrapper.containsMatchingElement(
          <>
            <div>
              error-test<button>&times;</button>
            </div>
            <div>
              info-test<button>&times;</button>
            </div>
          </>
        )
      ).toEqual(true);

      done();
    });



  });
});
