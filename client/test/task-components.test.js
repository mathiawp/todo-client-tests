// @flow

import * as React from 'react';
import { TaskList, TaskNew, TaskDetails, TaskEdit } from '../src/task-components';
import { type Task } from '../src/task-service';
import { shallow } from 'enzyme';
import { Form, Button, Column, Alert, Card, Row, NavBar } from '../src/widgets';
import { NavLink } from 'react-router-dom';

jest.mock('../src/task-service', () => {
  class TaskService {
    getAll() {
      return Promise.resolve([
        { id: 1, title: 'Les leksjon', done: false },
        { id: 2, title: 'Møt opp på forelesning', done: false },
        { id: 3, title: 'Gjør øving', done: false },
      ]);
    }

    get(id: number) {
      return Promise.resolve(
        { id: id, title: "Les leksjon", description: "Må huske å lese leksjonen!", done: false }
      )
    }

    update(task: object) {
      return Promise.resolve({ affectedRows: 1 })
    }

    create(title: string) {
      return Promise.resolve(4); // Same as: return new Promise((resolve) => resolve(4));
    }

    delete(id: number) {
      return Promise.resolve();
    }
  }
  return new TaskService();
});

describe('Task component tests', () => {
  test('TaskList draws correctly', (done) => {
    const wrapper = shallow(<TaskList />);

    // Wait for events to complete
    setTimeout(() => {
      expect(
        wrapper.containsAllMatchingElements([
          <NavLink to="/tasks/1">Les leksjon</NavLink>,
          <NavLink to="/tasks/2">Møt opp på forelesning</NavLink>,
          <NavLink to="/tasks/3">Gjør øving</NavLink>,
        ])
      ).toEqual(true);
      done();
    });
  });

  test('TaskNew correctly sets location on create', (done) => {
    const wrapper = shallow(<TaskNew />);

    wrapper.find(Form.Input).simulate('change', { currentTarget: { value: 'Kaffepause' } });
    // $FlowExpectedError
    expect(wrapper.containsMatchingElement(<Form.Input value="Kaffepause" />)).toEqual(true);

    wrapper.find(Button.Success).simulate('click');
    // Wait for events to complete
    setTimeout(() => {
      expect(location.hash).toEqual('#/tasks/4');
      done();
    });
  });

  test("TaskDetails draws task correctly", (done) => {
    const wrapper = shallow(<TaskDetails match={{ params: { id: 1 } }} />);

    setTimeout(() => {
      expect(wrapper.containsMatchingElement(
        <>
          <Card title="Task">
            <Row>
              <Column>Title:</Column>
              <Column>Les leksjon</Column>
            </Row>
            <Row>
              <Column>Description:</Column>
              <Column>Må huske å lese leksjonen!</Column>
            </Row>
            <Row>
              <Column>Done:</Column>
              <Column>
                <Form.Checkbox checked={false} />
              </Column>
            </Row>
          </Card>
          <Button.Success>
            Edit
          </Button.Success>
        </>
      ))
        .toEqual(true);

      wrapper.find(Button.Success).simulate('click');

      expect(location.hash).toEqual('#/tasks/1/edit');
      done();
    });
  })

  test("TaskDetails draws task correctly - snapshot", (done) => {
    const wrapper = shallow(<TaskDetails match={{ params: { id: 1 } }} />);

    setTimeout(() => {
      expect(wrapper).toMatchSnapshot();

      wrapper.find(Button.Success).simulate('click');

      expect(location.hash).toEqual('#/tasks/1/edit');
      done();
    });
  })

  test("TaskEdit draws task correctly - snapshot", (done) => {
    const wrapper = shallow(<TaskEdit match={{ params: { id: 1 } }} />);

    setTimeout(() => {
      expect(wrapper).toMatchSnapshot();
      done();
    });
  })



  test("TaskEdit draws task correctly and has functionality", (done) => {
    const wrapper = shallow(<TaskEdit match={{ params: { id: 1 } }} />);

    setTimeout(() => {
      expect(wrapper.containsMatchingElement(
        <>
          <Card title="Edit task">
            <Row>
              <Column>
                <Form.Label>Title:</Form.Label>
              </Column>
              <Column>
                <Form.Input
                  value="Les leksjon"
                />
              </Column>
            </Row>
            <Row>
              <Column>
                <Form.Label>Description:</Form.Label>
              </Column>
              <Column>
                <Form.Textarea value="Må huske å lese leksjonen!" />
              </Column>
            </Row>
            <Row>
              <Column>Done:</Column>
              <Column>
                <Form.Checkbox checked={false} />
              </Column>
            </Row>
          </Card>
          <Row>
            <Column>
              <Button.Success>Save</Button.Success>
            </Column>
            <Column>
              <Button.Danger>Delete</Button.Danger>
            </Column>
          </Row>
        </>
      ))
        .toEqual(true);

      wrapper.find(Form.Input).simulate('change', { currentTarget: { value: 'Joohoo' } });
      wrapper.find(Form.Textarea).simulate('change', { currentTarget: { value: 'Har lest leksjon nå :)' } });
      wrapper.find(Form.Checkbox).simulate('click');

      expect(wrapper.containsMatchingElement(<Form.Input value="Joohoo" />));
      expect(wrapper.containsMatchingElement(<Form.Textarea value="Har lest leksjon nå :)" />));
      expect(wrapper.containsMatchingElement(<Form.Checkbox checked={true} />));

      wrapper.find(Button.Success).simulate('click');
      expect(wrapper.containsMatchingElement(<Form.Input value="Joohoo" />));
      expect(wrapper.containsMatchingElement(<Form.Textarea value="Har lest leksjon nå :)" />));
      expect(wrapper.containsMatchingElement(<Alert />));

      wrapper.find(Button.Danger).simulate('click');

      setTimeout(() => {
        expect(location.hash).toEqual('#/tasks/');
        done();
      });

    });
  })
});
