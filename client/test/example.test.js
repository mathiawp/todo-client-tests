// @flow

import * as React from 'react';
import { Component } from 'react-simplified';
import { shallow } from 'enzyme';

class Hello extends Component {
    render() {
        return (
            <div>
                <b>Hello</b>
            </div>
        );
    }
}

describe('Hello tests', () => {
    test('Draws correctly', () => {
        const wrapper = shallow(<Hello />);

        expect(
            wrapper.matchesElement(
                <div>
                    <b>Hello</b>
                </div>
            )
        ).toEqual(true);
    });
});